$(function (){

    $('#registerForm').submit(function (evt){

        let valid = true;

        var name = $("#name").val();

        if(name === ''){
            $("#name").addClass('is-invalid');
            valid = false;
            $("#nameError").show();
        } else { $("#nameError").hide(); }

        var email = $("#email").val();
        var pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

        if(email === ''){
            $("#emailError").html("Email is required.");
            $("#email").addClass('is-invalid');
            valid = false;
            $("#emailError").show();
        } else if(email !=='' && !pattern.test(email)){
            $("#emailError").html("Email is invalid");
            $("#email").addClass('is-invalid');
            valid = false;
            $("#emailError").show();
        } else {
            $("#emailError").hide();
        }

        var password = $("#password").val();
        var confirm_password = $("#password-confirm").val();

        if (password === ''){
            $("#passwordError").html("Password is required.")
            $("#password").addClass('is-invalid');
            valid = false;
            $("#passwordError").show();
        } else { $("#passwordError").hide(); }


        if (password !== '' && password !== confirm_password){
            $("#passwordError").html("The password confirmation does not match.")
            $("#password").addClass('is-invalid');
            valid = false;
            $("#passwordError").show();
        }

        if (password !== '' && password === confirm_password){
            $("#passwordError").html("The password must be at least 8 characters.")
            $("#password").addClass('is-invalid');
            valid = false;
            $("#passwordError").show();
        }


        evt.preventDefault();
        if(!valid) {
            evt.preventDefault();
        }
    });
});
