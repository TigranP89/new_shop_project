$(function() {
    token_();
    getReplyId();
    CommentHeart();
    CommentReplyHeart();

    var product_user_id = $("#product_user_id").val();
    var user_id_che = $("#user_id").val();


    function GetFormattedDate(d) {
        let todayTime = new Date(d);
        let mth = todayTime.getMonth()+1;
        let month = (todayTime.getMonth() < 10 ? '0' : '') + mth;
        let day = (todayTime.getDate() < 10 ? '0' : '') + todayTime.getDate();
        let year = todayTime.getFullYear();
        return year + "-" + month + "-" + day;
    }

    $("#storeCommentAjax").submit('ajax', function (e){
        e.preventDefault();

        let token = $('meta[name="csrf-token"]').attr('content');
        let user_id = $("#user_id").val();
        let products_id = $("#products_id").val();
        let body = $("#body").val();

        $.ajax({
            url: "/storeCommentsAjax",
            method: 'POST',
            dataType: 'json',
            data: {
                token:token,
                user_id:user_id,
                products_id:products_id,
                body:body,
            },
            beforeSend:function(){
                $(document).find('span.error-text').text('');
            },
            success:function(response){
                if (response.status == 400){
                    $('#form_err_show').html('');
                    $('#form_err_show').addClass('alert alert-danger');
                    $.each(response.error, function (key, err_value){
                        $('#form_err_show').append('<li>' + err_value + '</li>');
                    });
                    setTimeout(function (){
                        $('#form_err_show').html('');
                        $('#form_err_show').removeClass('alert alert-danger');
                    }, 3000)
                } else {
                    $('#form_err_show').html('');
                    $('#form_err_show').hidden;
                    $('#success_message_show').addClass('alert alert-success');
                    $('#success_message_show').text(response.message);

                    let comment_box = '';
                    let reply_box = '';
                    let comments = response.comments;
                    let length = comments.length;
                    var reply_length ='';
                    let comment_box_second = '';
                    let comment_box_third = '';
                    let comment_box_fourth = '';
                    let comment_box_five = '';
                    let comment_box_first_one = '';
                    let comment_box_first_two = '';
                    let comment_box_third_one = '';


                    // reply_box +='<p>Hello</p>';
                    // $('#reply_box').html(reply_box);

                    for (let i = 0; i < length; i++){
                        reply_length = comments[i].comment_reply.length;

                        console.log(comments[i]);
                        let comment_box_first = `
                            <div class="border m-1 p-1">
                                <div class="card p-3 mt-2">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="user d-flex flex-row align-items-center">
                                            <img src="https://i.imgur.com/hczKIze.jpg" width="30" class="user-img rounded-circle mr-2">
                                            <span>
                                                <small class="font-weight-bold text-primary">` + comments[i].user.name + `</small>
                                                <div>
                                                    <small class="font-weight-bold">` + comments[i].body + `</small>
                                                </div>
                                            </span>
                                        </div>
                                        <small>` + GetFormattedDate(comments[i].created_at) + `</small>
                                    </div>
                                    <div class="action d-flex justify-content-between mt-2 align-items-center">
                                        <div class="reply px-4">
                                            <small><a data-bs-toggle="modal" href="#removeModal">Remove</a></small> <span class="dots"></span> <small><a class="reply_id" id="` + comments[i].id + `" data-bs-toggle="modal" href="#replyModal">Reply</a></small> <span class="dots"></span> <small><a data-bs-toggle="modal" href="#translateModal">Translate</a></small>
                                        </div>
                                        <div class="icons align-items-center" id="icons">`
                        if(comments[i].comment_heart_status == 0){
                            comment_box_first_one = `<i id="heart_` + comments[i].id + `" class='bx bx-heart heart_status'></i> </div>
                                        </div>
                                    </div>
                                <div id="reply_box">`
                        } else {
                            comment_box_first_one = `<i id="heart_` + comments[i].id + `" class='bx bxs-heart heart_status'></i> </div>
                                        </div>
                                    </div>
                                <div id="reply_box">`
                        }

                        if (reply_length){
                            comment_box_second = `
                                <div style="padding-top: 30px;">
                                <small class="font-weight-bold text-primary text-center">
                                    <a class="reply_btn"  id="reply_count_` + comments[i].id + `"  data-bs-toggle="collapse" href="#collapseExample_` + comments[i].id + `" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <snap>` + reply_length + `</snap> Replies
                                    </a>
                                </small>
                            </div>
                            <div class="collapse" id="collapseExample_` + comments[i].id + `">`;

                            for(let j = 0; j < reply_length; j++){
                                comment_box_third += `
                                    <div class="card p-3 mt-2 border">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="user d-flex flex-row align-items-center">
                                                <img src="https://i.imgur.com/hczKIze.jpg" width="30" class="user-img rounded-circle mr-2">
                                                <span>
                                                    <small class="font-weight-bold text-primary">Name</small>
                                                    <div>
                                                        <small class="font-weight-bold">` + comments[i].comment_reply[j].reply_body + `</small>
                                                    </div>
                                                </span>
                                            </div>
                                            <small>` + GetFormattedDate(comments[i].comment_reply[j].created_at) + `</small>
                                        </div>
                                        <div class="action d-flex justify-content-between mt-2 align-items-center">
                                            <div class="reply px-4">
                                                <small><a data-bs-toggle="modal" href="#removeModal">Remove</a></small> <span class="dots"></span>  <small><a data-bs-toggle="modal" href="#translateModal">Translate</a></small>
                                            </div>
                                            <div class="icons align-items-center">`;
                                if (comments[i].comment_reply[j].reply_heart_status == 0){
                                    comment_box_third_one = `<i id="reply_heart_` + comments[i].comment_reply[j].id + `" class='bx bx-heart reply_heart_status'></i>`
                                } else {
                                    comment_box_third_one = `<i id="reply_heart_` + comments[i].comment_reply[j].id + `" class='bx bxs-heart reply_heart_status'></i>`
                                }
                                comment_box_third_one +=`</div>
                                                                </div>
                                                            </div>
                                                            <input id="collapse_` + comments[i].comment_reply[j].id + `" type="hidden" name="depo_id" value="collapse_` + comments[i].comment_reply[j].id + `">`;

                            }
                            comment_box_fourth += `</div>`;
                        } else {
                            comment_box_second = `<div style="padding-top: 30px;">
                                <small class="font-weight-bold text-primary">
                                    <a class="" data-bs-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <snap>` + reply_length + `</snap> Replies
                                    </a>
                                </small>
                            </div>`;
                            comment_box_third = ``;
                            comment_box_third_one = ``;
                            comment_box_fourth = ``;
                        }
                        comment_box_five += ` </div>
                    <input type="hidden" class="form-control" id="comment_id" name="comment_id" value="{{$comment->id}}">
                </div>`;

                        comment_box_first += comment_box_first_one;
                        comment_box_first += comment_box_first_two;


                        comment_box_third += comment_box_third_one;
                        comment_box_second += comment_box_third;
                        comment_box_second += comment_box_fourth;

                        comment_box += comment_box_first;
                        comment_box += comment_box_second;
                        comment_box += comment_box_five;
                        $('#body').val('');
                    }
                    $('#comment_box').html(comment_box);

                    setTimeout(function (){
                        $('#success_message_show').html('');
                        $('#success_message_show').removeClass('alert alert-success');
                    }, 3000)
                }
            }
        });
    });

    $(document).on('click','.reply_id',function(e){
        e.preventDefault();
        var id = this.id;
        $('#comment_id_popup').val(id);
    });

    $("#storeCommentReplyAjax").submit('ajax', function (e){
        e.preventDefault();

        console.log(comment_id_popup+1);
        let user_id_c = $("#user_id").val();
        let reply_body = $("#reply_body").val();
        let comment_id = $("#comment_id_popup").val();

        console.log(user_id_c);
        console.log(comment_id);
        console.log(reply_body);

        $.ajax({
            url:"/storeCommentReplyAjax",
            method: 'POST',
            dataType: 'json',
            data: {
                user_id_c:user_id_c,
                comment_id:comment_id,
                reply_body:reply_body,
            },
            beforeSend:function(){

            },
            success:function(response){

                if (response.status == 400){

                }else {

                    var comment_replies = response.comment_replies;
                    $('#reply_body').val('');
                    $('#replyModal').modal('hide');
                }
            }
        })
    });

    if (product_user_id != user_id_che) {
        starRate();
    }
});
function getReplyId(){
    var id = $('a.reply_id').attr('id');
    $('#comment_id_popup').val(id);
}

function starRate(){
    $(".star" ).click(function() {

        $('.star').each(function(i, obj) {
            $(this).removeClass("bxs-star");
            $(this).addClass("bx-star");
        });

        var starId = $(this).attr('id');
        for(var i = 1; i <= 5; i++){
            if(i <= starId){
                $(`#${i}`).removeClass( "bx-star" ).addClass( "bxs-star" );
            }
        }

        let user_id = $("#user_id").val();
        let products_id = $("#products_id").val();

        $.ajax({
            url: "/starRate",
            method: "POST",
            data: {
                starId:starId,
                user_id:user_id,
                products_id:products_id,
            },
            beforeSend:function(){

            },
            success:function (response){
                $('#userRate').val('');
                var rates = response.rates;
                var rateAvg = response.rateRoundUp;
                var rateCount = response.rateCount;
                var userRate = rates.rating;

                $('#userRate').html(userRate);
                $('#allRates').html(rateCount);


                for(var i = 1; i <= 5; i++){
                    if(i <= rateAvg){
                        $(`#${i}`).removeClass( "bx-star" ).addClass( "bxs-star" );
                    }
                }

            }
        });
    });
}

function CommentHeart(){
    $(document).on('click','.heart_status',function(e){
        e.preventDefault();

        var heartStatus ='';
        var user_id = $("#user_id").val();
        var comment_id = $(this).attr("id");

        var myClass = $(this).attr("class");
        if (myClass == 'bx bx-heart heart_status'){
            $(this).removeClass( "bx bx-heart heart_status" ).addClass( "bx bxs-heart heart_status" );
            heartStatus = true;
        } else {
            $(this).removeClass( "bx bxs-heart heart_status" ).addClass( "bx bx-heart heart_status" );
            heartStatus = false;
        }

        $.ajax({
            url: "/commentHeart",
            method: "POST",
            data: {
                heartStatus:heartStatus,
                user_id:user_id,
                comment_id:comment_id,
            },
        })
    });
}

function CommentReplyHeart(){
    $(document).on('click','.reply_heart_status',function(e){
        e.preventDefault();

        var heartReplyStatus ='';
        var user_id = $("#user_id").val();
        var reply_id = $(this).attr("id");

        var myClass = $(this).attr("class");
        console.log(myClass);
        if (myClass == 'bx bx-heart reply_heart_status'){
            $(this).removeClass( "bx bx-heart reply_heart_status" ).addClass( "bx bxs-heart reply_heart_status" );
            heartReplyStatus = true;
        } else {
            $(this).removeClass( "bx bxs-heart reply_heart_status" ).addClass( "bx bx-heart reply_heart_status" );
            heartReplyStatus = false;
        }

        $.ajax({
            url: "/commentReplyHeart",
            method: "POST",
            data: {
                heartReplyStatus:heartReplyStatus,
                user_id:user_id,
                reply_id:reply_id,
            },
        })
    });
}
