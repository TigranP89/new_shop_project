$(function (){
    var reservationData = {};

    var requiredFieldsCount = 0;

    var STRIPE_KEY = '';
    var unitPrice = $("#totalAmount").val();
    var itemQuantity = $("#addQuantity").val();
    var itemId = $("#itemId").val();

    let $form = $(".require-validation");
    $('#pay-by-stripe').bind('click', function (e){

        STRIPE_KEY = $(this).data('stripe-publishable-key');

        let $form = $(".require-validation"),
            inputSelector = ['input[type=email]', 'input[type=password]',
                'input[type=text]', 'input[type=file]',
                'textarea'
            ].join(', '),
            $inputs = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('div.error'),
            valid = true;
        $errorMessage.addClass('hide');
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            let $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('hide');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            e.preventDefault();
            Stripe.setPublishableKey(STRIPE_KEY);
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
    })


    function stripeResponseHandler(status, response) {

        if(response.error) {
            setTimeout(function(){
                $('.error').removeClass('hide').find('.alert').text(response.error.message);
            }, 500);
        } else {

                $.ajax({
                    url: "/stripe",
                    method: "POST",
                    data:{
                        itemId:itemId,
                        stripeToken: response['id'],
                        unitPrice: unitPrice,
                        itemQuantity:itemQuantity,
                    },
                    beforeSend:function(){

                    },
                    success:function (response){
                        console.log(this);
                        location.reload();
                    }
                })

        }
    }

    // function formValidation(){
    //     requiredFieldsCount = 0;
    //     requiredFieldsArr.forEach(function(item) {
    //         if($('.debitCredit #'+item).val() === ""){
    //             requiredFieldsCount++;
    //             $(".debitCredit #" + item).addClass('invalid-input');
    //         }else{
    //             $(".debitCredit #" + item).removeClass('invalid-input');
    //             reservationData[item] = $(".debitCredit #" + item).val();
    //         }
    //     });
    // }

})
