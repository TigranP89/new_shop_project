$(function (){
    token_();

    $('#search').on('keyup', function(e){
        e.preventDefault();
        search();
    });

    search();

    $(document).on('click','.home-page-label a', function (e) {
        e.preventDefault();
        page = $(this).attr('href').split('page=')[1];
        search(page);
    })

    function search(page=1){
        var search = $('#search').val();


        $.ajax({
            url:'/search',
            method:"GET",
            dataType: 'json',
            data:{
                search:search,
                page:page
            },
            success:function(response){
                console.log(response);
                var searchTable = '';
                var paginationLable = '';
                var current_page = response.products.current_page;
                var from = response.products.from;
                $('#searchTable').html('');

                $.each(response.products.data, function (index,value){

                    searchTable +=
                        `
                        <tr>
                            <td>`+ from++ +`</td>
                            <td>`

                            if (value.product_image.name !== 'No-image-available.png'){
                                searchTable +=`<img src="/storage/products/`+ value.product_image.name+`" width="100px">`
                            } else {
                                searchTable +=`<img src="/storage/no_image/No-image-available.png" width="100px">`
                            }

                    searchTable +=`</td>
                            <td>`+ value.title +`</td>
                            <td>`

                            if (value.quantity == 0){
                                searchTable +=`<p style="color: red;">Out of stock</p>`
                            } else {
                                searchTable += `<p>`+ value.quantity +`</p>`
                            }

                    searchTable +=`</td>
                            <td style="word-break: break-word">`+ value.description +`</td>
                            <td>
                                <form action="" method="POST">
                                    <a class="btn btn-info" href="products/`+ value.id +`">Show</a>
                                </form>
                            </td>
                        </tr>
                    `
                    $('#searchTable').html(searchTable);
                });

                $.each(response.products.links, function (index,value){
                    if (value.url!=null){

                        if (value.active === true){
                            paginationLable +=`<li class="page-item active home-page-label"><a class="page-link" href="`+value.url+`">`+value.label+`</a></li>`
                            $('#pagination').html(paginationLable);
                        } else {
                            paginationLable +=`<li class="page-item home-page-label"><a class="page-link" href="`+value.url+`">`+value.label+`</a></li>`
                            $('#pagination').html(paginationLable);
                        }
                    }
                });
            },
        })
    }
});



