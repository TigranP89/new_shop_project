$(function() {
    var filesArr = [];
    var formData = new FormData();
    token_();

    // Multiple images preview with JavaScript
    var multiImgPreview = function(input, imgPreviewPlaceholder) {

        if (input.files) {
            var filesAmount = input.files.length;
            for (let i = 0; i < filesAmount; i++) {
                var file = new Object();//
                file.name = input.files[i].name;//
                var reader = new FileReader();

                var extension = file.name.substr(file.name.lastIndexOf("."));
                var allowedExtensionsRegx = /(\.jpg|\.jpeg|\.png|\.gif|\.svg)$/i;
                var isAllowed = allowedExtensionsRegx.test(extension);

                if(isAllowed){
                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        file.data = event.target.result;//

                    }

                    formData.append('file', input.files[i]); ///
                    reader.readAsDataURL(input.files[i]);
                    filesArr.push(file);//
                } else {
                    alert("Extension not good only try with GIF, JPG, PNG, JPEG");
                    return false;
                }




            }
        }
        console.log(formData);
    };

    $('#images').on('change', function() {
        multiImgPreview(this, 'div.imgPreview');
    });

    $("#storeAjax").submit('ajax', function (e){
        e.preventDefault();
        var $this = $(this);
        // var formData = new FormData(this);

        let token = $('meta[name="csrf-token"]').attr('content');
        let user_id = $("#user_id").val();
        let title = $("#title").val();
        let price = $("#price").val();
        let quantity = $("#quantity").val();
        let description = $("#description").val();

        $.ajax({
            url: "/storeAjax",
            method: 'POST',
            dataType: 'json',
            data: {
                token:token,
                filesArr:filesArr,
                user_id:user_id,
                title:title,
                price:price,
                quantity:quantity,
                description:description,
            },
            beforeSend:function(){
                $(document).find('span.error-text').text('');
            },
            success:function(response){
                if (response.status == 400){
                    $('#form_err').html('');
                    $('#form_err').addClass('alert alert-danger');
                    $.each(response.error, function (key, err_value){
                        $('#form_err').append('<li>' + err_value + '</li>');
                    });
                } else {
                    $('#form_err').html('');
                    $('#form_err').hidden;
                    $('#success_message').addClass('alert alert-success');
                    $('#success_message').text(response.message);
                    setTimeout(function(){ location.replace("/products"); }, 1000);
                }
            }
        });
    });

    $('#cancelButton').click(function() {
        location.replace("/products");
        return false;
    });
});
