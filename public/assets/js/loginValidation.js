$(function (){
    $('#loginForm').submit(function (evt){
        let valid = true;

        var logEmail = $("#email").val();

        if(logEmail === ''){
            $("#emailLogError").html("<strong>The email field is required.</strong>");
            $("#email").addClass('is-invalid');
            valid = false;
            $("#emailLogError").show();
        } else { $("#emailLogError").hide();}

        var logPass = $("#password").val();

        if(logPass === ''){
            $("#passLogError").html("<strong>The password field is required.</strong>");
            $("#password").addClass('is-invalid');
            valid = false;
            $("#passLogError").show();s
        } else { $("#passLogError").hide();}

        if(!valid) {
            evt.preventDefault();
        }
    });
});
