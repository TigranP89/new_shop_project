$(function (){
    token_();


    $(document).on('input', '#addQuantity',function (){
        var addQuantity = $("#addQuantity").val();
        console.log(addQuantity);
        $.ajax({
            url:"/addToCart",
            method: 'POST',
            dataType: 'json',
            data: {
                addQuantity:addQuantity,
            },
            beforeSend:function(){

            },
            success:function(response){

            },
        })
    });


    $(".remove-from-cart").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");

        console.log(id);
        $.ajax({
            url: '/remove-from-cart',
            method: "post",
            data: {
                id: id,
            },
            success: function (response) {
                window.location.reload();
            }
        });

    });

    $(".update-cart").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var quantity = $("#addQuantity").val();
        $.ajax({
            url: '/update-cart',
            method: "patch",
            data: {
                id: id,
                quantity: quantity,
            },
            success: function (response) {
                window.location.reload();
            }
        });
    });
});
