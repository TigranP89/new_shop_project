<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\CommentHeartController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PaginationController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\StripeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/search', [HomeController::class, 'search']);
//Route::post('/search', [HomeController::class, 'search']);
//Route::post('/', [HomeController::class, 'pagination']);

Route::resource('products', ProductController::class);
Route::post('/storeAjax', [ProductController::class,'storeAjax']);
Route::delete('/deleteImage/{id}', [ProductController::class,'deleteImage'])->name('deleteImage');

Route::get('/cart', [CartController::class, 'cart'])->name('cart');
Route::get('/addToCart/{id}', [CartController::class, 'addToCart'])->name('addToCart');
Route::get('/buyNow/{id}', [CartController::class, 'buyNow'])->name('buyNow');

Route::post('/storeCommentsAjax', [CommentController::class,'storeCommentsAjax']);
Route::post('/storeCommentReplyAjax', [CommentController::class,'storeCommentReplyAjax']);

Route::post('/starRate', [RatingController::class,'starRate']);

Route::post('/commentHeart', [CommentHeartController::class,'commentHeart']);
Route::post('/commentReplyHeart', [CommentHeartController::class,'commentReplyHeart']);

Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
Route::get('/profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
Route::post('/profile/update', [ProfileController::class, 'update'])->name('profile.update');
Route::post('/remove-from-cart', [ProductController::class, 'removeFromCart'])->name('remove');
Route::patch('/update-cart', [ProductController::class, 'updateFromCart'])->name('updateFromCart');

Route::post('stripe', [StripeController::class, 'stripePost'])->name('stripe.post');
//Route::get('checkout', [StripeController::class, 'checkout'])->name('stripe.checkout');
//Route::post('stripe', [StripeController::class, 'stripePost'])->name('stripe.post');
