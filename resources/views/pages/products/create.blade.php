@extends('layouts.app')

@section('content')

{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            <strong>Whoops!</strong> There were some problems with your input.<br><br>--}}
{{--            <ul>--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <li>{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    @endif--}}

    <style>
        .imgPreview img {
            padding: 8px;
            max-width: 80px;
        }
    </style>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
{{--                            @if (session('status'))--}}
{{--                                <div class="alert alert-success" role="alert">--}}
{{--                                    {{ session('status') }}--}}
{{--                                </div>--}}
{{--                            @endif--}}
                            <div class="col-6">
                                <h3 class="fw-normal text-secondary fs-4 text-uppercase mb-4">Add new item</h3>
                                    {{--Validation output--}}
                                <ul id="form_err"></ul>
                                <div id="success_message"></div>
                            </div>
                            <form class="needs-validation" name="store" id="storeAjax">
                                @csrf

                                <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{auth()->user()->id}}">
                                <div class="col-3 mb-3">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                                    <snap class="text-danger error-text title_error" id="title_error"></snap>
                                </div>
                                <div class="col-3 mb-3">
                                    <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                                    <snap class="text-danger error-text price_error" id="price_error"></snap>
                                </div>
                                <div class="col-3 mb-3">
                                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                                    <snap class="text-danger error-text quantity_error" id="quantity_error"></snap>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="3"></textarea>
                                    <snap class="text-danger error-text description_error" id="description_errorr"></snap>
                                </div>
                                {{--Image preview--}}
                                <div class="col-md-12 mb-1">
                                    <div class="user-image mb-3 text-center">
                                        <div class="imgPreview">
                                        </div>
                                    </div>

                                    <div class="input-group mb-3">
                                        <input type="hidden" name="imageNames" value="" id="imageNames">
                                        <input type="file" name="imageFile[]" class="form-control" id="images" multiple="multiple">
                                        <label class="input-group-text" for="images">Upload</label>
                                        <snap class="text-danger error-text imageFile_error"></snap>
                                    </div>
                                </div>
                                <div class="col-12 mt-b">
                                    <button class="btn btn-primary float-end">Submit</button>
                                    <button id="cancelButton" type="button" class="btn btn-outline-secondary float-end me-2">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
