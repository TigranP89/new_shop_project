@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container mt-5 mb-5">
        <div class="buttons d-flex flex-row my-3 gap-3"> <button class="btn btn-outline-dark" id="goBack">Back</button></div>
        <div class="card">
            <div class="row g-0">
                <div class="col-md-6 border-end">
                    <div class="d-flex flex-column justify-content-center">

                        <div class="main_image">
                            @if(!empty($product->productImage->name))
                                <img src="{{asset('/storage/products/'. $product->productImage->name)}}" id="main_product_image" width="350">
                            @else
                                <img src="{{asset('/storage/no_image/No-image-available.png')}}" width="100px">
                            @endif
                        </div>
                        <div class="thumbnail_images">
                            <ul id="thumbnail">
                                @if(!empty($product->productImages))
                                    @foreach($product->productImages as $key => $images)
                                        <li><img class="changeImage" onclick="changeImage(this)" src="{{asset('/storage/products/'. $images->name)}}" width="70"></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-3 right-side">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>{{$product->title}}</h3> <span class="heart"><i class='bx bx-heart'></i></span>
                        </div>
                        <div class="mt-2 pr-3 content">
                            <p>{{$product->description}}</p>
                        </div>
                        <h3>{{$product->price}} $</h3>
                        <div class="ratings d-flex flex-row align-items-center">

                            <div class="d-flex flex-row" id="star">
                                @if(((double)number_format($product->ratings->avg('rating'), 0, ".", "")) >= 1){{--One of metrhods get 'ratings' avg--}}
                                    <i class='bx bxs-star star' aria-hidden="true" id="1"></i>
                                @else
                                    <i class='bx bx-star star' aria-hidden="true" id="1"></i>
                                @endif

                                @if(((double)number_format($product->ratings->avg('rating'), 0, ".", "")) >= 2)
                                    <i class='bx bxs-star star' aria-hidden="true" id="2"></i>
                                @else
                                    <i class='bx bx-star star' aria-hidden="true" id="2"></i>
                                @endif

                                @if(((double)number_format($product->ratings->avg('rating'), 0, ".", "")) >= 3)
                                    <i class='bx bxs-star star' aria-hidden="true" id="3"></i>
                                @else
                                    <i class='bx bx-star star' aria-hidden="true" id="3"></i>
                                @endif

                                @if(((double)number_format($product->ratings->avg('rating'), 0, ".", "")) >= 4)
                                    <i class='bx bxs-star star' aria-hidden="true" id="4"></i>
                                @else
                                    <i class='bx bx-star star' aria-hidden="true" id="4"></i>
                                @endif

                                @if(((double)number_format($product->ratings->avg('rating'), 0, ".", "")) >= 5)
                                    <i class='bx bxs-star star' aria-hidden="true" id="5"></i>
                                @else
                                    <i class='bx bx-star star' aria-hidden="true" id="5"></i>
                                @endif
                            </div>
                            <span>
                                @if(!empty($product->ratings->count()))
                                    <span id="allRates">{{$product->ratings->count()}}</span> {{--One of metrhods get 'ratings' cont--}}
                                @else
                                    <span id="allRates">0</span>
                                @endif
                                reviews
                            </span>
                            @if(!Auth::guest())
                                @if($product->user_id == auth()->user()->id)
                                    <snap id="selfRate" class="m-3">(Yon cann`t rate your product)</snap>
                                @else
                                    <span id="selfRate">(your rate is:
                                    @if(!empty($product->rating->rating))
                                            <span id="userRate">{{$product->rating->rating}}</span>)
                                        @else
                                            <span id="userRate">0</span>)
                                        @endif
                                </span>
                                @endif
                            @else
                                <snap id="selfRate" class="m-3">(Guests can not rate product)</snap>
                            @endif

                        </div>
                        @if(!Auth::guest())
                            @if($product->user_id !== auth()->user()->id)
                                @if($product->quantity == "0")
                                    <div class="buttons d-flex flex-row mt-5 gap-3">
                                        <a href="#" class="btn btn-outline-dark" style="padding-top: 12px;">Buy Now</a>
                                        <a href="#" class="btn btn-dark" style="padding-top: 12px;">Add to Basket</a>
                                    </div>
                                    <div>
                                        <p style="color: red;">Out of stock</p>
                                    </div>
{{--                                <div class="row g-3 align-items-center p-2" style="width: 100px;">--}}
{{--                                    <div class="col-auto">--}}
{{--                                        <input type="number" class="form-control" id="addQuantity" min="1" value="{{$product->quantity}}">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                @else
                                    <div class="buttons d-flex flex-row mt-5 gap-3">
                                        <a href="{{ route('buyNow',$product->id) }}" class="btn btn-outline-dark" style="padding-top: 12px;">Buy Now</a>
                                        <a href="{{ route('addToCart',$product->id) }}" class="btn btn-dark" style="padding-top: 12px;">Add to Basket</a>
                                    </div>
                                @endif
                            @endif
                        @else
                            @if($product->quantity == "0")
                                <div class="buttons d-flex flex-row mt-5 gap-3">
                                    <a href="#" class="btn btn-outline-dark" style="padding-top: 12px;">Buy Now</a>
                                    <a href="#" class="btn btn-dark" style="padding-top: 12px;">Add to Basket</a>
                                </div>
                                <div>
                                    <p style="color: red;">Out of stock</p>
                                </div>
                            @else
                                <div class="buttons d-flex flex-row mt-5 gap-3">
                                    <a href="{{ route('buyNow',$product->id) }}" class="btn btn-outline-dark" style="padding-top: 12px;">Buy Now</a>
                                    <a href="{{ route('addToCart',$product->id) }}" class="btn btn-dark" style="padding-top: 12px;">Add to Basket</a>
                                </div>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row g-0">
            <div class="d-flex flex-column justify-content-center">
                <h5 class="card-title">Comments</h5>

                    <ul id="form_err_show"></ul>
                    <div id="success_message_show"></div>
                @if(!Auth::guest())
                    <form name="comments" class="needs-validation mb-0" id="storeCommentAjax">
                        @csrf
                        <input type="hidden" class="form-control" id="product_user_id" name="product_user_id" value="{{$product->user_id}}">
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{auth()->user()->id}}">
                        <input type="hidden" class="form-control" id="products_id" name="products_id" value="{{$product->id}}">

                        <label for="author" class="form-label mt-6 text-sm font-medium text-gray-700">Text</label>
                        <textarea name="body" id="body" class="form-control mt-1 py-2 px-3 block w-full border border-grey-400 rounded-md shadow-sm"></textarea>

                        <input id="submitComment" type="submit" value="Add Comment" class="btn btn-success float-end my-3" name="submit">
                    </form>
                @else
                    <div>Guests can not leave comments </div>
                @endif

            </div>
        </div>
        <div class="mt-6" id="comment_box">
            @foreach($product->comments as $key => $comment)
                <div class="border m-1 p-1">
                    <div class="card p-3 mt-2">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="user d-flex flex-row align-items-center">
                                <img src="https://i.imgur.com/hczKIze.jpg" width="30" class="user-img rounded-circle mr-2">
                                <span>
                                    <small class="font-weight-bold text-primary">{{ $comment->user->name }}</small>
                                    <div>
                                        <small class="font-weight-bold">{{ $comment->body }}</small>
                                    </div>
                                </span>
                            </div>
                            <small>{{ $comment->created_at->diffForHumans() }}</small>
                        </div>
                        <div class="action d-flex justify-content-between mt-2 align-items-center">
                            <div class="reply px-4">
                                <small><a data-bs-toggle="modal" href="#removeModal">Remove</a></small> <span class="dots"></span> <small><a class="reply_id" id="{{$comment->id}}" data-bs-toggle="modal" href="#replyModal">Reply</a></small> <span class="dots"></span> <small><a data-bs-toggle="modal" href="#translateModal">Translate</a></small>
                            </div>
                            <div class="icons align-items-center" id="icons">
                                @if(!$comment->commentHeart->isEmpty())
                                    @foreach($comment->commentHeart as $heartSatus)
                                        @if(!Auth::guest())
                                            @if($heartSatus->user_id === auth()->user()->id)
                                                <i id="heart_{{$heartSatus->comment_id}}" class='bx bxs-heart heart_status'></i>
                                            @endif
                                        @else
                                            <i id="heart_{{$heartSatus->comment_id}}" class='bx bxs-heart heart_status'></i>
                                        @endif
                                    @endforeach
                                @else
                                    <i id="heart_{{$comment->id}}" class='bx bx-heart heart_status'></i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="reply_box">
                        @if(count($comment->commentReply) != 0)
                            <div style="padding-top: 30px;">
                                <small class="font-weight-bold text-primary text-center">
                                    <a class="reply_btn"  id="reply_count_{{$comment->id}}"  data-bs-toggle="collapse" href="#collapseExample_{{$comment->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <snap>{{count($comment->commentReply)}}</snap> Replies
                                    </a>
                                </small>
                            </div>
                            <div class="collapse" id="collapseExample_{{$comment->id}}">
                                @foreach($comment->commentReply as $key2 => $reply)

                                    <div class="card p-3 mt-2 border">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="user d-flex flex-row align-items-center">
                                                <img src="https://i.imgur.com/hczKIze.jpg" width="30" class="user-img rounded-circle mr-2">
                                                <span>
                                                    <small class="font-weight-bold text-primary">{{  $reply->user->name }}</small>
                                                    <div>
                                                        <small class="font-weight-bold">{{ $reply->reply_body }}</small>
                                                    </div>
                                                </span>
                                            </div>
                                            <small>{{ $reply->created_at->diffForHumans() }}</small>
                                        </div>
                                        <div class="action d-flex justify-content-between mt-2 align-items-center">
                                            <div class="reply px-4">
                                                <small><a data-bs-toggle="modal" href="#removeModal">Remove</a></small> <span class="dots"></span>  <small><a data-bs-toggle="modal" href="#translateModal">Translate</a></small>
                                            </div>
                                            <div class="icons align-items-center">
                                                @if($reply->reply_heart_status == "0")
                                                    <i id="reply_heart_{{$reply->id}}" class='bx bx-heart reply_heart_status'></i>
                                                @else
                                                    <i id="reply_heart_{{$reply->id}}" class='bx bxs-heart reply_heart_status'></i>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <input id="collapse_{{ $reply->id }}" type="hidden" name="depo_id" value="collapse_{{ $reply->id }}">
                                @endforeach
                            </div>
                        @else
                            <div style="padding-top: 30px;">
                                <small class="font-weight-bold text-primary">
                                    <a class="" data-bs-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <snap>{{ count($comment->commentReply) }}</snap> Replies
                                    </a>
                                </small>
                            </div>
                        @endif
                    </div>
                    <input type="hidden" class="form-control" id="comment_id" name="comment_id" value="{{$comment->id}}">
                </div>

            @endforeach
        </div>
    </div>
    <div class="modal fade" id="replyModal" tabindex="-1" aria-labelledby="replyModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="commentReply" class="needs-validation mb-0" id="storeCommentReplyAjax">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-3">
                            <label for="reply_body" class="col-form-label">Message:</label>
                            <textarea class="form-control" id="reply_body"></textarea>
                        </div>
                        <input type="hidden" class="form-control" id="comment_id_popup" name="comment_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <input id="submitCommentReply" type="submit" value="Send message" class="btn btn-primary" name="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


<script >
    function changeImage(element) {
        var main_prodcut_image = document.getElementById('main_product_image');
        main_prodcut_image.src = element.src;
    }

    $(function() {
        $('button#goBack').on('click', function(e){
            e.preventDefault();
            window.history.back();
        });
    });
</script>
