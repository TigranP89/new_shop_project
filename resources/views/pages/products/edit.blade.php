@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="col-6">
                                <h3 class="fw-normal text-secondary fs-4 text-uppercase mb-4">Edit item</h3>
                            </div>

                            <form action="{{route('products.update',$product->id)}}" class="needs-validation" name="update" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$product->title}}">
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col-md-6 mb-3">
                                            <input type="text" class="form-control" id="price" name="price" placeholder="Price" value="{{$product->price}}">
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col-md-6 mb-3">
                                            <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity" value="{{$product->quantity}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-3">
                                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="3">{{$product->description}}</textarea>
                                </div>

                                <div class="w-100"></div>

                                <div class="col-md-12 mb-1">
                                    <div class="custom-file">
                                        <input type="file" name="imageFile[]" class="custom-file-input" id="images" multiple="multiple">
                                        <label class="custom-file-label" for="images">Upload</label>
                                    </div>
                                </div>

                                <div class="w-100"></div>

                                <div class="col-12 mt-b">
                                    <input id="submitInput" type="submit" value="Edit" class="btn btn-primary float-end" name="submit">
                                    <button type="button" class="btn btn-outline-secondary float-end me-2" id="goBackEdit">Cancel</button>
                                </div>
                            </form>

                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th><span class="ml-2">Image</span></th>
                                        <th><span class="ml-2">Name</span></th>
                                    </tr>
                                    @if(!empty($product->productImages))
                                        @foreach($product->productImages as $key => $image)
                                            <tr>
                                                <td>
                                                    <img src="{{asset('/storage/products/'. $image->name)}}" width="70"></li>
                                                </td>
                                                <td class="col-3">
                                                    <form action="{{ route('deleteImage',$image->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(function() {
        $('button#goBackEdit').on('click', function(e){
            e.preventDefault();
            window.history.back();
        });
    });
</script>
