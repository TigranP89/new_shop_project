@extends('layouts.app')



@section('content')
<style>
    .ms-3{
        margin-right: 10px;
    }
</style>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container" style="padding: 30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-default">
                    {{--                    <div class="card-header-heading">--}}
                    {{--                        <div class="row">--}}
                    {{--                            @if (session('status'))--}}
                    {{--                                <div class="alert alert-success" role="alert">--}}
                    {{--                                    {{ session('status') }}--}}
                    {{--                                </div>--}}
                    {{--                            @endif--}}

                    {{--                            @if ($message = Session::get('success'))--}}
                    {{--                                <div class="alert alert-success">--}}
                    {{--                                    <p>{{ $message }}</p>--}}
                    {{--                                </div>--}}
                    {{--                            @endif--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="card-body p-4">
                        <div class="row">
                            <div class="col-lg-7">
                                <h5 class="mb-3"><a href="#!" class="text-body"><i class="fas fa-long-arrow-alt-left me-2"></i>Continue shopping</a></h5>
                                <hr>
                                <div class="d-flex justify-content-between align-items-center mb-4">
                                    <div>
                                        <p class="mb-1">Shopping cart</p>
                                        <p class="mb-0">You have 4 items in your cart</p>
                                    </div>
                                </div>

                                <div id="divCheckbox" style="display: none;">
                                    {{ $total = 0 }}
                                    {{$shipping = 20}}
                                </div>
                                @if(Session::has('cart'))
                                    @foreach(session('cart') as $id => $item)
                                        <div id="divCheckbox" style="display: none;">
                                            {{ $total += $item['quantity']*$item['price']}}
                                        </div>

                                        <input id="itemId" type="hidden" value="{{$item['id']}}">
                                        <div class="card mb-3">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-between">
                                                    <div class="d-flex flex-row align-items-center">
                                                        <div>
                                                            <img src="{{asset('/storage/products/'. $item['photo'])}}" class="img-fluid rounded-3" alt="Shopping item" style="width: 65px;">
                                                        </div>
                                                        <div class="ms-3">
                                                            <h5>{{$item['name']}}</h5>
                                                            <p class="small mb-0 text-break">{{$item['description']}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex flex-row align-items-center">
                                                        <div style="width: 61px;height: 23px;">
{{--                                                            <input type="number" class="form-control" id="addQuantity" value="{{$item['quantity']}}" min="1" max="{{$item['itemQuantity']}}">--}}
                                                            <select class="form-select form-select-sm" id="addQuantity">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10+">10+</option>
                                                            </select>
                                                        </div>
                                                        <div style="width: 50px;text-align: center;">
                                                            <p class="mb-0"  id="price">{{$item['price']}}</p>
                                                        </div>
                                                        <div style="width: 50px;text-align: center;">
                                                            <p class="mb-0" >{{$item['quantity']*$item['price']}}</p>
                                                        </div>
                                                        <a href="#!" style="color: #cecece;" data-id="{{$id}}" class="pe-3 update-cart"><i class="fas fa-sync"></i></a>
                                                        <a href="#!" style="color: #cecece;" data-id="{{$id}}" class="remove-from-cart"><i class="fas fa-trash-alt"></i></a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <div class="col-lg-5">
                                <div class="card bg-primary text-white rounded-3">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center mb-4">
                                            <h5 class="mb-0">Card details</h5>
{{--                                            <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-6.webp" class="img-fluid rounded-3" style="width: 45px;" alt="Avatar">--}}
                                        </div>

                                        <p class="small mb-2">Card type</p>
                                        <a href="#!" type="submit" class="text-white"><i class="fab fa-cc-mastercard fa-2x me-2"></i></a>
                                        <a href="#!" type="submit" class="text-white"><i class="fab fa-cc-visa fa-2x me-2"></i></a>
                                        <a href="#!" type="submit" class="text-white"><i class="fab fa-cc-amex fa-2x me-2"></i></a>
                                        <a href="#!" type="submit" class="text-white"><i class="fab fa-cc-paypal fa-2x"></i></a>

                                        <form
                                            role="form"
                                            action="{{ route('stripe.post') }}"
                                            method="POST"
                                            data-cc-on-file="false"
                                            {{--                                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"--}}
                                            id="payment-form"
                                            class="mt-4 require-validation debitCredit">
                                            @csrf
                                            <div class="form-row row">
                                                <div class="form-outline form-white mb-4 form-group required">
                                                    <input autocomplete='off' type="text" id="NameOnCard" class="form-control form-control-lg" siez="4" placeholder="Cardholder's Name" />
                                                    <label class="form-label" for="NameOnCard">Cardholder's Name</label>
                                                </div>
                                            </div>

                                            <div class="form-row row">
                                                <div class="form-outline form-white mb-4 form-group required">
                                                    <input autocomplete='off' type="text" id="CardNumber" class="form-control form-control-lg card-number" siez="20" placeholder="1234 5678 9012 3457" minlength="19" maxlength="19" />
                                                    <label class="form-label" for="CardNumber">Card Number</label>
                                                </div>
                                            </div>

                                            <div class="form-row row">
                                                <div class="row mb-4">
                                                    <div class="col-md-6">
                                                        <div class="form-outline form-white expiration required">
                                                            <input autocomplete='off' type="text" id="ExpiryMonth" class="form-control form-control-lg card-expiry-month"  placeholder="MM" size="2" minlength="2" maxlength="2" />
                                                            <label class="form-label" for="ExpiryMonth">Expiration Month</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-outline form-white expiration required">
                                                            <input autocomplete='off' type="text" id="ExpiryYear" class="form-control form-control-lg card-expiry-year"  placeholder="YYYY" size="4" minlength="4" maxlength="4" />
                                                            <label class="form-label" for="ExpiryYear">Expiration Year</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-outline form-white form-group cvc required">
                                                            <input autocomplete='off' type="password" id="CVC" class="form-control form-control-lg card-cvc" placeholder="&#9679;&#9679;&#9679;" size="1" minlength="3" maxlength="3" />
                                                            <label class="form-label" for="CVC">CVC</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="my-4">

                                            <div class="d-flex justify-content-between">
                                                <p class="mb-2">Subtotal</p>
                                                <p class="mb-2">${{$total}}</p>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <p class="mb-2">Shipping</p>
                                                <p class="mb-2">${{$shipping}}</p>
                                            </div>

                                            <div class="d-flex justify-content-between mb-4">
                                                <p class="mb-2">Total(Incl. taxes)</p>
                                                <p class="mb-2">${{$total2 = $total + $shipping}}</p>
                                            </div>

                                            <button type="submit" class="btn btn-info btn-block btn-lg" id="pay-by-stripe" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}">
                                                <div class="d-flex justify-content-between">
                                                    <input type="hidden" id="totalAmount" value="{{$total2}}">
                                                    <span>${{$total2}}</span>
                                                    <span>Checkout <i class="fas fa-long-arrow-alt-right ms-2"></i></span>
                                                </div>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
