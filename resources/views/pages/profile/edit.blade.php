@extends('layouts.app')

@section('content')

    <style>
        body {
            background: rgb(248, 250, 252)
        }

        .form-control:focus {
            box-shadow: none;
            border-color: #BA68C8
        }

        .profile-button {
            background: rgb(99, 39, 120);
            box-shadow: none;
            border: none
        }

        .profile-button:hover {
            background: #682773
        }

        .profile-button:focus {
            background: #682773;
            box-shadow: none
        }

        .profile-button:active {
            background: #682773;
            box-shadow: none
        }

        .back:hover {
            color: #682773;
            cursor: pointer
        }

        .labels {
            font-size: 11px
        }

        .add-experience:hover {
            background: #f1eff1;
            color: #fff;
            cursor: pointer;
            border: solid 1px #BA68C8
        }
    </style>
    <div class="buttons d-flex flex-row my-3 gap-3"> <button class="btn btn-outline-dark" id="goBackProfileEdit">Back</button></div>
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                    <img class="rounded-circle mt-5" width="150px" src="{{asset('/storage/avatars/'. Auth::user()->avatar)}}">
                    <span class="font-weight-bold">{{$user->name}}</span><span class="text-black-50">{{$user->email}}</span><span> </span>
                </div>
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Profile Settings</h4>
                    </div>
                    <form action="{{route('profile.update')}}" class="needs-validation" name="updateProfile" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row mt-2">
                            <div class="col-md-12"><label class="labels">Name</label><input type="text" class="form-control" placeholder="name" value="{{$user->name}}" name="name"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label class="labels">E-Mail Address</label>
                                <input type="text" id="profileEmail" name="email" class="form-control" placeholder="enter email id" value="{{$user->email}}">
                            </div>
                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Phone Number</label>
                                    <input type="text" id="profilePhone" name="phone" class="form-control" placeholder="enter phone number" value="{{$user->profile->phone}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Phone Number</label>
                                    <input type="text" id="profilePhone" name="phone" class="form-control" placeholder="enter phone number" value="">
                                </div>
                            @endif

                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Mobile Number</label>
                                    <input type="text" id="profileMobile" name="mobile" class="form-control" placeholder="enter mobile number" value="{{$user->profile->mobile}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Mobile Number</label>
                                    <input type="text" id="profileMobile" name="mobile" class="form-control" placeholder="enter mobile number" value="">
                                </div>
                            @endif

                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Address Line 1</label>
                                    <input type="text" id="profileAddressLine1" name="address_line_1" class="form-control" placeholder="enter address line 1" value="{{$user->profile->address_line_1}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Address Line 1</label>
                                    <input type="text" id="profileAddressLine1" name="address_line_1" class="form-control" placeholder="enter address line 1" value="">
                                </div>
                            @endif

                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Address Line 2</label>
                                    <input type="text" id="profileAddressLine2" name="address_line_2" class="form-control" placeholder="enter address line 2" value="{{$user->profile->address_line_2}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Address Line 2</label>
                                    <input type="text" id="profileAddressLine2" name="address_line_2" class="form-control" placeholder="enter address line 2" value="">
                                </div>
                            @endif

                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Postcode</label>
                                    <input type="text" id="profilePostcode" name="postcode" class="form-control" placeholder="enter postcode" value="{{$user->profile->postcode}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Postcode</label>
                                    <input type="text" id="profilePostcode" name="postcode" class="form-control" placeholder="enter postcode" value="">
                                </div>
                            @endif

                            @if($user->profile !== null)
                                <div class="col-md-12">
                                    <label class="labels">Birthday</label>
                                    <input type="text" id="profileBirthday" name="birthday" class="form-control" placeholder="enter birthday" value="{{$user->profile->birthday}}">
                                </div>
                            @else
                                <div class="col-md-12">
                                    <label class="labels">Birthday</label>
                                    <input type="text" id="profileBirthday" name="birthday" class="form-control" placeholder="enter birthday" value="">
                                </div>
                            @endif
                        </div>
                        <label>Update Profile Image</label>
                        <input type="file" name="avatar" class="form-control">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="mt-5 text-center"><button type="submit" class="btn btn-primary profile-button" type="button">Save Profile</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(function() {
        $('button#goBackProfileEdit').on('click', function(e){
            e.preventDefault();
            window.history.back();
        });
    });
</script>
