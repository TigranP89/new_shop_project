@extends('layouts.app')

@section('content')

    <style>
        body {
            background: rgb(248, 250, 252)
        }

        .form-control:focus {
            box-shadow: none;
            border-color: #BA68C8
        }

        .profile-button {
            background: rgb(99, 39, 120);
            box-shadow: none;
            border: none
        }

        .profile-button:hover {
            background: #682773
        }

        .profile-button:focus {
            background: #682773;
            box-shadow: none
        }

        .profile-button:active {
            background: #682773;
            box-shadow: none
        }

        .back:hover {
            color: #682773;
            cursor: pointer
        }

        .labels {
            font-size: 11px
        }

        .add-experience:hover {
            background: #f1eff1;
            color: #fff;
            cursor: pointer;
            border: solid 1px #BA68C8
        }
    </style>
    <div class="buttons d-flex flex-row my-3 gap-3"> <button class="btn btn-outline-dark" id="goBackProfile">Back</button></div>
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">


            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                    <img class="rounded-circle mt-5" width="150px" src="{{asset('/storage/avatars/'. Auth::user()->avatar)}}">
                    <span class="font-weight-bold">{{$user->name}}</span><span class="text-black-50">{{$user->email}}</span><span> </span>
                </div>
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Profile Information</h4>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12"><label class="labels">Name</label><p>{{$user->name}}</p></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12"><label class="labels">E-Mail Address</label><p>{{$user->email}}</p></div>
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Phone Number</label><p>{{$user->profile->phone}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Phone Number</label><p>No info</p></div>
                        @endif
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Mobile Number</label><p>{{$user->profile->mobile}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Mobile Number</label><p>No info</p></div>
                        @endif
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Address Line 1</label><p>{{$user->profile->address_line_1}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Address Line 1</label><p>No info</p></div>
                        @endif
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Address Line 2</label><p>{{$user->profile->address_line_2}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Address Line 2</label><p>No info</p></div>
                        @endif
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Postcode</label><p>{{$user->profile->postcode}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Postcode</label><p>No info</p></div>
                        @endif
                        @if($user->profile !== null)
                            <div class="col-md-12"><label class="labels">Birthday</label><p>{{$user->profile->birthday}}</p></div>
                        @else
                            <div class="col-md-12"><label class="labels">Birthday</label><p>No info</p></div>
                        @endif
                    </div>
                    <div class="mt-5 text-center"><a target="__blank" class="btn btn-primary profile-button" type="button" href="{{ route('profile.edit') }}" disabled>Edit</a></div>
                </div>
            </div>

       </div>
    </div>
@endsection
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(function() {
        $('button#goBackProfile').on('click', function(e){
            e.preventDefault();
            window.history.back();
        });
    });
</script>
