@extends('layouts.app')

@section('content')
    <div class="container" style="padding: 30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header-heading">
                        <div class="row">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div>
{{--                                <form class="float-right m-3" method="GET" action="">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input type="search" class="form-control" name="search" placeholder="Search..."/>--}}
{{--                                    </div>--}}

{{--                                    <button class="btn btn-outline-dark" type="submit">Search</button>--}}
{{--                                    <a href="{{route('home')}}" class="btn btn-outline-dark">--}}
{{--                                        Reset Home--}}
{{--                                    </a>--}}

{{--                                </form>--}}
                                <form class="float-right m-3" method="GET" action="">
                                    <div class="form-group">
                                        <input type="search" id="search" class="form-control" name="search" placeholder="Search..."/>
                                    </div>
                                </form>
                                @if(!Auth::guest())
                                    <div class="float-right m-3">
                                        <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><span class="ml-2">No</span></th>
                                        <th><span class="ml-2">Image</span></th>
                                        <th><span class="ml-2">Name</span></th>
                                        <th><span class="ml-2">Quantity</span></th>
                                        <th><span class="ml-2">Details</span></th>
                                        <th><span class="ml-2">Action</span></th>
                                    </tr>
                                </thead>
                                <tbody id="searchTable">
{{--                                    @foreach ($products as $product)--}}
{{--                                        <tr>--}}
{{--                                            <td>{{ ++$i }}</td>--}}
{{--                                            <td>--}}
{{--                                                @if($product->productImage->name !== 'No-image-available.png')--}}
{{--                                                    <img src="{{asset('/storage/products/'. $product->productImage->name)}}" width="100px">--}}
{{--                                                @else--}}
{{--                                                    <img src="{{asset('/storage/no_image/No-image-available.png')}}" width="100px">--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
{{--                                            <td>{{ $product->title }}</td>--}}
{{--                                            <td>--}}
{{--                                                @if($product->quantity == "0")--}}
{{--                                                    <p style="color: red;">Out of stock</p>--}}
{{--                                                @else--}}
{{--                                                    {{ $product->quantity }}--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
{{--                                            <td style="word-break: break-word">{{ $product->description }}</td>--}}
{{--                                            <td>--}}
{{--                                                <form action="{{ route('products.destroy',$product->id) }}" method="POST">--}}
{{--                                                    @csrf--}}
{{--                                                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>--}}
{{--                                                </form>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}

                                </tbody>
                            </table>
                            <nav>
                                <ul class="pagination" id="pagination">

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
