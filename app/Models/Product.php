<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'title',
        'price',
        'quantity',
        'description',
        'views_count'
    ];
// One of metrhods get 'ratings' count
//    protected $withCount = [
//        'ratings'
//    ];

    public function productImage()
    {
        return $this->hasOne(Image::class,"products_id")->oldest();
    }

    public function productImages()
    {
        return $this->hasMany(Image::class,"products_id");
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, "products_id")->latest();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, "products_id")->latest();
    }

    public function rating()
    {
        return $this->hasOne(Rating::class,"products_id")->latest();
    }

//    public function getRateCountAttribute()
//    {
//        $this->rating()->count();
//    }
}
