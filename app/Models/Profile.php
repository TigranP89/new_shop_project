<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'email',
        'phone',
        'mobile',
        'address_line_1',
        'address_line_2',
        'postcode',
        'birthday',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
