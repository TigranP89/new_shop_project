<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'products_id',
        'parent_id',
        'body'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->latest();
    }

    public function commentReply()
    {
        return $this->hasMany(CommentReply::class, "comment_id");
    }

    public function commentHeart()
    {
        return $this->hasMany(CommentHeart::class, "comment_id");
    }
}
