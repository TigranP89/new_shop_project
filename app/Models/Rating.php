<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'products_id',
        'rating'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->latest();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
