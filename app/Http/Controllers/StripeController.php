<?php

namespace App\Http\Controllers;

use App\Jobs\StripeJob;
use App\Models\Product;
use Illuminate\Http\Request;
use Session;
use Stripe;

class StripeController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {

        $stripe = new Stripe\StripeClient(config('stripe.secret'));

        $stripe->charges->create([
            "amount" => $request->unitPrice * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "This payment is test"
        ]);

        $product = Product::find($request->itemId);
        $quantity =  (int)$product->quantity - (int)$request->itemQuantity;
        Product::where('id', $request->itemId)->update(['quantity' => $quantity]);

        $request->session()->forget('cart');
        Session::flash('success', 'Payment successful!');

        return response()->json([
            'status'=> true,
        ]);
    }
}
