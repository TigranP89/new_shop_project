<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $id = auth()->user()->id;
        $user = User::where('id', $id)->with("profile")->first();

        return view('pages.profile.profile',compact('user'));
    }

    public function edit(){
        $id = auth()->user()->id;
        $user = User::where('id', $id)->with("profile")->first();

        return view('pages.profile.edit',compact('user'));
    }

    public function update(Request $request){


//        dd($request->all());
        $input = $request->all();
        $user = User::where('id',auth()->user()->id)->first();

        $user->update($input);

        $profile = Profile::where('user_id',auth()->user()->id)->first();

        if ($profile !== null){
            $profile->user_id = $user->id;
            $profile->email = $user->email;
            $profile->phone = $input['phone'] ?? '';
            $profile->mobile = $input['mobile'] ?? '';
            $profile->address_line_1 = $input['address_line_1'] ?? '';
            $profile->address_line_2 = $input['address_line_2'] ?? '';
            $profile->postcode = $input['postcode'] ?? '';
            $profile->birthday = $input['birthday'] ?? '';
            $profile->update();
        } else {
            $profile = new Profile;
            $profile->user_id = $user->id;
            $profile->email = $user->email;
            $profile->phone = $input['phone'] ?? '';
            $profile->mobile = $input['mobile'] ?? '';
            $profile->address_line_1 = $input['address_line_1'] ?? '';
            $profile->address_line_2 = $input['address_line_2'] ?? '';
            $profile->postcode = $input['postcode'] ?? '';
            $profile->birthday = $input['birthday'] ?? '';
            $profile->save();
        }

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');

            $path = public_path() . '/storage/avatars/';
            $filename = $user->id . '_' .'avatar'. '_' . date('YmdHis') . '.' . $avatar->getClientOriginalExtension();
            $avatar->move($path,$filename);


            $user->avatar = $filename;
            $user->update();
        }

        return redirect()->route('profile');

    }
}
