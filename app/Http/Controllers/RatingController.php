<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rating;
class RatingController extends Controller
{
    public function starRate(Request $request){
        $starRequsert = $request->all();

        $rate = new Rating();
        $rate->user_id = $starRequsert['user_id'];
        $rate->products_id = $starRequsert['products_id'];
        $rate->rating = $starRequsert['starId'];
        $rate->save();

        $rates = Rating::where('products_id', $starRequsert['products_id'])->latest()->first();

        $rateAvg = Rating::select('id')
                        ->orderby('products_id')
                        ->avg('rating');

        $rateRoundUp = (double)number_format($rateAvg, 0, ".", "");

        $rateCount = Rating::where('products_id', $starRequsert['products_id'])->count();

        return response([
            'rates'=>$rates,
            'rateRoundUp'=>$rateRoundUp,
            'rateCount'=>$rateCount,
        ]);
    }
}
