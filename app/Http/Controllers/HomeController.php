<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

//        $search = $request->input('search');
//        if($search != ""){
//            $products = Product::where('title','LIKE', '%'.$search.'%')
//                ->orWhere('description','LIKE', '%'.$search.'%')
//                ->orWhere('price','LIKE', '%'.$search.'%')
//                ->orWhere('quantity','LIKE', '%'.$search.'%')
//                ->latest()->paginate(5);
//        } else {
//            $products = Product::latest()->paginate(5);
//        }
//
//
//        return view('home',compact('products'))
//            ->with('i', (request()->input('page', 1) - 1) * 5);

        return view('home');
    }


    public function search(Request $request)
    {

        $link = '';

        $search = $request->search;
        $page = $request->page;

        if($search !== ""){

            $products = Product::where('title','LIKE', '%'.$search.'%')
                ->orWhere('description','LIKE', '%'.$search.'%')
                ->orWhere('price','LIKE', '%'.$search.'%')
                ->orWhere('quantity','LIKE', '%'.$search.'%')
                ->with("productImage")
                ->latest()->paginate(5, ['*'], 'page', $page);

        } else {
            $products = Product::with("productImage")
                ->latest()->paginate(5, ['*'], 'page', $page);
        }

        return response()->json([
            'products'=>$products,
            'link'=>$link,
        ]);
    }
}
