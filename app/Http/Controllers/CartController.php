<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function cart()
    {
        return view('pages.products.cart');
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::with("productImages")->find($id);
        $image = Image::where('products_id', $id)->first();

        $cart = $request->session()->get('cart');

        if(!$cart ){
            $cart = [
                $id =>[
                    "id" => $product->id,
                    "name" => $product->title,
                    "description" => $product->description,
                    "quantity" => 1,
                    "itemQuantity" => $product->quantity,
                    "price" => $product->price,
                    "photo" => $image->name
                ]

            ];
            $request->session()->put('cart', $cart);
            return back();
        }

        if (isset($cart[$id])){
            $cart[$id]['quantity']++;
            Session::put('cart', $cart);
            return back();
        } else {
            $cart[$id] = [
                "id" => $product->id,
                "name" => $product->title,
                "description" => $product->description,
                "quantity" => 1,
                "itemQuantity" => $product->quantity,
                "price" => $product->price,
                "photo" => $image->name
            ];
            $request->session()->put('cart', $cart);
            return back();
        }
    }

    public function removeFromCart(Request $request)
    {

        if($request->id) {
            $cart = $request->session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $request->session()->put('cart', $cart);
            }
            $request->session()->flash('success', 'Product removed successfully');
        }
    }

    public function updateFromCart(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
//            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function buyNow(Request $request, $id)
    {

        $product = Product::with("productImages")->find($id);
        $image = Image::where('products_id', $id)->first();

        $cart = $request->session()->get('cart');

        $cart = [
            $id =>[
                "id" => $product->id,
                "name" => $product->title,
                "description" => $product->description,
                "quantity" => 1,
                "itemQuantity" => $product->quantity,
                "price" => $product->price,
                "photo" => $image->name
            ]
        ];
        $request->session()->put('cart', $cart);
        return redirect('/cart');
    }
}
