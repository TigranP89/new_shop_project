<?php

namespace App\Http\Controllers;

use App\Jobs\CommentJob;
use App\Models\CommentReply;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Comment;
use App\Models\Product;


class CommentController extends Controller
{
    public function storeComments(Request $request, Product $product, $id)
    {
        $request->validate([
            'body' => ['required', 'min:20'],
        ]);

        $input = $request->all();

        $comment = new Comment();
        $comment->user_id = $input['user_id'];
        $comment->products_id = $id;
        $comment->parent_id = 1;
        $comment->body =  $input['body'];
        $comment->save();


        return back();
    }

    public function storeCommentsAjax(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'body' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validator->errors()->toArray()
            ]);
        }else{
            $input = $request->all();

            $comment = new Comment();
            $comment->user_id = $input['user_id'];
            $comment->products_id = $input['products_id'];
            $comment->parent_id = 1;
            $comment->body =  $input['body'];
            $comment->save();

//            $job = new CommentJob($comment);
//            $this->dispatch($job);

//            CommentJob::dispatch($comment);

            CommentJob::dispatch($comment)->delay(20);


            $comments = Comment::with("user")
                ->with("product")
                ->with("commentReply")
                ->with("commentHeart")
                ->where('products_id', $input['products_id'])->orderBy('id', 'desc')->get();

            return response()->json([
                'status'=>200 ,
                'message'=>'New comment was created.',
                'comments'=>$comments
            ]);
        }

    }

    public function storeCommentReplyAjax(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reply_body' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validator->errors()->toArray()
            ]);
        }else {
            $input = $request->all();

            $comment_reply = new CommentReply();
            $comment_reply->user_id = $input['user_id_c'];
            $comment_reply->comment_id = $input['comment_id'];
            $comment_reply->reply_body = $input['reply_body'];
            $comment_reply->save();

            $comment_replies = CommentReply::with("user")
                    ->with("comment")
                    ->where('comment_id', $input['comment_id'])->get();
            return response()->json([
                'status'=>200 ,
                'message'=>'New comment was created.',
                'comment_replies'=>$comment_replies
            ]);
        }
    }
}
