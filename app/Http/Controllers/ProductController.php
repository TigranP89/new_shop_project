<?php

namespace App\Http\Controllers;

use App\Jobs\ProductJob;
use App\Models\CommentHeart;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    static $search;

//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user()->id;
        ProductController::$search = $request->input('search');

        if(ProductController::$search != ""){

            $products = Product::where('user_id', $user)
                ->where(function($query) {
                    $query->where('title','LIKE', '%'.ProductController::$search.'%')
                        ->orWhere('description','LIKE', '%'.ProductController::$search.'%')
                        ->orWhere('price','LIKE', '%'.ProductController::$search.'%')
                        ->orWhere('quantity','LIKE', '%'.ProductController::$search.'%');
                })
                ->with("productImage")
                ->latest()->paginate(5);
        } else {
            $products = Product::where('user_id', $user)->with("productImage")->latest()->paginate(5);
        }

        return view('pages.products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'min:3'],
            'price' => ['required', 'numeric'],
            'quantity' => ['required', 'integer'],
            'description' => ['required','min:20'],
            'imageFile' => 'required',
            'imageFile.*' => ['required','image','mimes:jpg,png,jpeg,gif,svg|max:2048']
        ]);


        $input = $request->all();

        $product = new Product;
        $product->title = $input['title'];
        $product->user_id = $input['user_id'];
        $product->price = $input['price'];
        $product->quantity = $input['quantity'];
        $product->description = $input['description'];
        $product->save();

        if ($request->hasfile('imageFile')) {
            foreach($request->file('imageFile') as $kay=>$file)
            {
                $fileName = date('YmdHis') . "_" .$kay . "_" . $file->getClientOriginalName();
                $file->storeAs('products', $fileName, 'public');

                $image = new Image;
                $image->user_id = $input['user_id'];
                $image->products_id = $product->id;
                $image->name = $fileName;
                $image->image_path = public_path() . '/storage/products/'. $fileName;
                $image->save();
            }
        }

        return redirect()->route('products.index')
            ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Product::where('id', $id)->first()->increment('views_count');

        $product = Product::with("productImages")
            ->with("comments")
            ->with("ratings")
            ->where('id', $id)->first();

        return view('pages.products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->first();
        $images = Image::where('products_id', $id)->get();

        return view('pages.products.edit',compact('product','images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Image $images)
    {
        $request->validate([
            'title' => ['required', 'min:3'],
            'price' => ['required', 'numeric'],
            'quantity' => ['required', 'integer'],
            'description' => ['required','min:20'],
            'imageFile.*' => ['image','mimes:jpg,png,jpeg,gif,svg|max:2048']
        ]);

        $input = $request->all();
        $product->update($input);

        if ($request->hasfile('imageFile')) {
            foreach($request->file('imageFile') as $kay=>$file)
            {
                $fileName = date('YmdHis') . "_" .$kay . "_" . "2" . "_" .$file->getClientOriginalName();
                $file->storeAs('products', $fileName, 'public');

                $images = new Image;
                $images->user_id = $product->user_id;
                $images->products_id = $product->id;
                $images->name = $fileName;
                $images->image_path = public_path() . '/storage/products/'. $fileName;
                $images->save();
            }
        }


        return redirect()->route('products.index')
            ->with('success','Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,Product $product)
    {
        $images = Image::where('products_id', $product->id)->get();


        if($images){
            foreach ($images as $image){
                unlink(public_path().'/storage/products/'. $image->name);
            }
        }

        $product->delete();
        Image::where('products_id', $product->id)->delete();
        Comment::where('products_id', $product->id)->delete();

        return redirect()->route('products.index')
            ->with('success','Product deleted successfully.');
    }

    public function storeAjax(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'min:3'],
            'price' => ['required', 'numeric'],
            'quantity' => ['required', 'integer'],
            'description' => ['required', 'min:20']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validator->errors()->toArray()
            ]);
        }else{
            $input = $request->all();

            $product = new Product;
            $product->title = $input['title'];
            $product->user_id = $input['user_id'];
            $product->price = $input['price'];
            $product->quantity = $input['quantity'];
            $product->description = $input['description'];
            $product->views_count = 0;
            $product->save();

            $filesArr = $input['filesArr'];

            if (count($filesArr) > 0) {
                foreach ($filesArr as $kay => $file) {
                    $fileName = date('YmdHis') . "_" . $kay . "_" . $file['name'];

                    $path = str_replace("\\", "/", public_path());

                    $image_tmp = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file['data']));
                    file_put_contents($path . '/storage/products/' . $fileName, $image_tmp);

                    $images = new Image;
                    $images->user_id = $input['user_id'];
                    $images->products_id = $product->id;
                    $images->name = $fileName;
                    $images->image_path = $path . '/storage/products/' . $fileName;
                    $images->save();
                }

                return response()->json([
                    'status'=>200 ,
                    'message'=>'New product was created.'
                ]);
            }
        }

    }

    public function deleteImage($id)
    {
        $images = Image::where('id', $id)->get();
        if($images){
            foreach ($images as $image){
                unlink(public_path().'/storage/products/'. $image->name);
            }
        }
        Image::where('id', $id)->delete();

        return back();
    }
}
