<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CommentHeart;
use App\Models\Comment;
use App\Models\CommentReply;

class CommentHeartController extends Controller
{
    public function commentHeart(Request $request)
    {
        $heartRequest = $request->all();
        $comment_id = str_replace('heart_','',$heartRequest['comment_id']);

        if ($heartRequest['heartStatus'] == 'true'){
            $heartbool = '1';
        } else {
            $heartbool = '0';
        }
        $comment_heart_availability_of_assessment = CommentHeart::where('comment_id', $comment_id)->where('user_id', auth()->user()->id)->value('id');

        if ($comment_heart_availability_of_assessment != null){
            CommentHeart::where('comment_id', $comment_id)->where('user_id', auth()->user()->id)->update(['comment_heart_status' => $heartbool]);

            $comment_like_status = CommentHeart::where('comment_id', $comment_id)->where('user_id', auth()->user()->id)->value('comment_heart_status');
            if ($comment_like_status == '0'){
                CommentHeart::where('comment_heart_status',$comment_like_status)->delete();
            }
        } else {
            $comment_heart_status = new CommentHeart;
            $comment_heart_status->user_id = auth()->user()->id;
            $comment_heart_status->comment_id = $comment_id;
            $comment_heart_status->comment_heart_status = $heartbool;
            $comment_heart_status->save();
        }
    }

    public function commentReplyHeart(Request $request)
    {
        $heartReplyRequest = $request->all();

        $reply_id = str_replace('reply_heart_','',$heartReplyRequest['reply_id']);

        if ($heartReplyRequest['heartReplyStatus'] == 'true'){
            $heartbool = '1';
        } else {
            $heartbool = '0';
        }

        CommentReply::where('id', $reply_id)->update(['reply_heart_status' => $heartbool]);
    }
}
