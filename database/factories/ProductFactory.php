<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => rand(1,50),
            'title' => $this->faker->text(20),
            'description' => $this->faker->text(20),
            'price' => rand(1,100),
            'quantity' => rand(1,100),
            'views_count' => rand(1,100),
        ];
    }
}
